-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2016 at 04:27 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `labexam8`
--

-- --------------------------------------------------------

--
-- Table structure for table `examoccrud`
--

CREATE TABLE IF NOT EXISTS `examoccrud` (
`id` int(212) NOT NULL,
  `name` varchar(323) NOT NULL,
  `phone` int(121) NOT NULL,
  `email` varchar(212) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examoccrud`
--

INSERT INTO `examoccrud` (`id`, `name`, `phone`, `email`) VALUES
(1, 'manim', 132, 'ab@gmail.com'),
(3, 'ayman', 1343435335, 'aym26@gmail.com'),
(6, 'jo', 4324325, 'gh@gmail.com'),
(7, 'farazi', 434234, 'ab@gmail.com'),
(8, 'nabila', 84343253, 'nabila@gmail.com'),
(9, 'faria', 1567334455, 'faria@gmail.com'),
(10, 'nazia', 1654223344, 'nazia@gmail.com'),
(11, 'arabi', 1755332244, 'arabi@gmail.com'),
(12, 'jo45', 1844335566, 'gh@gmail.com'),
(13, 'nabilajk', 84343253, 'nabila@gmail.com'),
(14, 'farazi amin', 434234, 'ab@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `examoccrud`
--
ALTER TABLE `examoccrud`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `examoccrud`
--
ALTER TABLE `examoccrud`
MODIFY `id` int(212) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
