<?php

session_start();

include_once('../vendor/autoload.php');
use App\BITM\exam;

$labexam=new exam();
//$allItem=$labexam->index();







    if(array_key_exists('itemPerPage',$_SESSION)) {
        if (array_key_exists('itemPerPage', $_GET)) {
            $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
        }
    }
    else{
        $_SESSION['itemPerPage']=5;
    }

    $itemPerPage=$_SESSION['itemPerPage'];
    //Utility::d($itemPerPage);
    $totalItem=$labexam->count();

    $totalPage=ceil($totalItem/$itemPerPage);

    $pagination="";
    //Utility::d($_GET);

    if(array_key_exists('pageNumber',$_GET)){
        $pageNumber=$_GET['pageNumber'];
    }else{
        $pageNumber=1;
    }
    for($i=1;$i<=$totalPage;$i++){
        $class=($pageNumber==$i)?"active":"";
        $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
    }

    $pageStartFrom=$itemPerPage*($pageNumber-1);
    $prevPage=$pageNumber-1;
    $nextPage=$pageNumber+1;
    $allItem = $labexam->paginator($pageStartFrom, $itemPerPage);
//var_dump($allItem);






?>

<!DOCTYPE html>
<html>
<head>
    <title>All List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
</head>
<body>
<br>
<h1>ALL LIST</h1>



<div class="container">

    <a href="create.php" class="btn btn-primary" role="button">Insert Again</a>
<br><br>
    <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
    <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
    <br>
    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }?></div>
    <br>
    <form role="form">
        <div class="form-group">
            <label>How many items per page? (select one):</label>
            <select class="form-control" name="itemPerPage">
                <option<?php if($itemPerPage==5){?> selected <?php }?>>5</option>
                <option<?php if($itemPerPage==10){?> selected <?php }?>>10</option>
                <option<?php if($itemPerPage==15){?> selected <?php }?>>15</option>
                <option<?php if($itemPerPage==20){?> selected <?php }?>>20</option>
                <option<?php if($itemPerPage==25){?> selected <?php }?>>25</option>
            </select>
            <br>
            <button type="submit">Go!</button>
        </div>
    </form>
    <br><br>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>NAME</th>
                <th>PHONE</th>
                <th>EMAIL</th>
                <th>action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allItem as $item){
                $sl++;
                ?>
                <td><?php echo $sl+$pageStartFrom?></td>
                <td><?php echo $item['id']?></td>
                <td><?php echo $item['name']?></td>
                <td><?php echo $item['phone']?></td>
                <td><?php echo $item['email']?></td>

                <td><a href="view.php?id=<?php echo $item['id'] ?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $item['id']?>" class="btn btn-info" role="button">Edit</a>
                    <a href="email.php?id=<?php echo $item['id']?>" class="btn btn-info" role="button">Email </a>
                    <a href="delete.php?id=<?php echo $item['id']?>" class="btn btn-info" role="button">Delete</a>
                </td>


            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

       <ul class="pagination">
                <?php if($pageNumber>1){?>
                    <li><a href="index.php?pageNumber=<?php echo $prevPage?>">Prev</a></li>
                <?php }?>
                <?php echo $pagination?>
                <?php if($pageNumber<$totalPage){?>
                    <li><a href="index.php?pageNumber=<?php echo $nextPage?>">Next</a></li>
                <?php }?>
            </ul>


</div>


<script>
    $('#message').show().delay(2000).fadeOut()
</script>
<script>
    $( function() {
        var availableTags = [
            <?php echo $comma_separated_title?>
        ];
        $( "#title" ).autocomplete({
            source: availableTags
        });
    } );
</script>
<script>
    $( function() {
        var availableTags = [
            <?php echo $comma_separated_desc?>
        ];
        $( "#desc" ).autocomplete({
            source: availableTags
        });
    } );
</script>

</body>
</html>



