<?php

include_once('../vendor/autoload.php');
use App\BITM\exam;



$labexam = new exam();
$labexam->prepare($_GET);
$singleItem=$labexam->view();

//Utility::dd($singleBook);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>



<body>

<div class="container">
    <form role="form" action="update.php" method="post" class="form-group">
        <input type="hidden" name="id" class="form-control" value="<?php echo $singleItem['id']?>">

        <div id="form-group">
            <label>  name</label>
            <input type="text" name="name" class="form-control" value="<?php echo $singleItem['name']?>">
        </div>
        <br> <br>

        <div id="form-group">
            <label>  phone number</label>
            <input type="text" name="phone" class="form-control" value="<?php echo $singleItem['phone']?>">
        </div>
        <br> <br>
        <div id="form-group">
            <label> Email</label>
            <input type="text" name="email" class="form-control" value="<?php echo $singleItem['email']?>">
        </div>
        <br>
        <button type="submit">Submit</button>


    </form>
</div>
</body>
</html>